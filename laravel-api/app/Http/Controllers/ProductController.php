<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Events\ProductSaved;
use Illuminate\Http\Request;
use Elasticsearch\ClientBuilder;


class ProductController extends Controller
{

    private $client;

    public function __construct()
    {
        $this->client = ClientBuilder::create()->setHosts(['http://laravel-poc-yoss_elasticsearch_1:9200'])->build();
    }

    // Affiche la liste des produits indexer dans elasticSearch


    public function see()
{

    $params = [
        'index' => 'products',
        'body'  => [
            'query' => [
                'match_all' => (object) []
            ]
        ]
    ];

    $response =  $this->client->search($params);

    return response()->json($response);
}
    
    // Affiche la liste des produits
    public function index()
    {
        $products = Product::all();
        
        // Convert the tags from JSON string to array for each product
        $products->transform(function($product) {
            $product->tags = json_decode($product->tags);
            return $product;
        });
    
        return response()->json($products, 200);
    }
    
        
        public function loadProducts()
        {
            $sortedProducts = $this->search2(); // La fonction qui effectue la recherche dans Elasticsearch

            // Extraire les identifiants des produits
            $sortedProductIds = array_map(function($product) {
                return $product['id'];
            }, $sortedProducts);
            $products = Product::whereIn('id', $sortedProductIds)->get();

            $sortedProducts = $products->sortBy(function($product) use ($sortedProductIds) {
                return array_search($product->id, $sortedProductIds);
            });
            
            return response()->json($sortedProducts->values()->all());
        }



    public function save(Request $request)
{
    // Créer un nouveau produit avec les données fournies
    $product = Product::create($request->all());

    // create new tags sous forme : 'name' => $tag, 'score' => 0,
    $tagsArray = json_decode( $product->tags, true);
    $newTagsArray = [];
    foreach ($tagsArray as $tag) {
        $newTagsArray[] = [
            'name' => $tag,
            'score' => 0,
        ];
    }
    $product_elastic =  $product;
    $product_elastic->tags = $newTagsArray;
   
    // Émettre l'événement pour indexer le produit dans Elasticsearch
    event(new ProductSaved($product_elastic));

    // Répondre avec un message de succès et les détails du produit
    return response()->json(['message' => 'Product created successfully.', 'product' => $product], 201);
}


    // Affiche le détail d'un produit spécifique

    public function show($id)
    {
        $product = Product::findOrFail($id);
        $product->tags = json_decode($product->tags);
        $tags = $product->tags;

        $this->main($tags);

        return response()->json($product, 200);
    }

    // Met à jour le produit dans la base de données
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'description' => 'nullable',
        ]);

        $product = Product::findOrFail($id);
        $product->update($validatedData);

        return response()->json(['message' => 'Product updated successfully.', 'product' => $product], 200);
    }

    // Supprime un produit de la base de données
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        return response()->json(['message' => 'Product deleted successfully.'], 200);
    }



public function search()
    {
        $params = [
            'index' => 'products',
            'body' => [
                '_source' => ['id', 'name'], // Limiter les champs source retournés à 'id' et 'name'
                'query' => [
                    'match_all' => new \stdClass()
                ],
                'sort' => [
                    [
                        '_script' => [
                            'type' => 'number',
                            'script' => [
                                'lang' => 'painless',
                                'source' => 'int totalScore = 0; for (int i = 0; i < params._source.tags.length; ++i) { totalScore += params._source.tags[i].score; } return totalScore;'
                            ],
                            'order' => 'desc'
                        ]
                    ]
                ]
            ]
        ];

        try {
            $response = $this->client->search($params);
            $filteredResponse = collect($response['hits']['hits'])->map(function ($hit) {
                return [
                    'id' => $hit['_source']['id'] ?? null,
                    'name' => $hit['_source']['name'] ?? null

                ];
            });
            return response()->json($filteredResponse);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }



    public function search2()
{
    $params = [
        'index' => 'products',
        'body' => [
            '_source' => ['id', 'name'], // Limiter les champs source retournés à 'id' et 'name'
            'query' => [
                'match_all' => new \stdClass()
            ],
            'sort' => [
                [
                    '_script' => [
                        'type' => 'number',
                        'script' => [
                            'lang' => 'painless',
                            'source' => 'int totalScore = 0; for (int i = 0; i < params._source.tags.length; ++i) { totalScore += params._source.tags[i].score; } return totalScore;'
                        ],
                        'order' => 'desc'
                    ]
                ]
            ]
        ]
    ];

    try {
        $response = $this->client->search($params);
        $filteredResponse = collect($response['hits']['hits'])->map(function ($hit) {
            return [
                'id' => $hit['_source']['id'] ?? null,
                'name' => $hit['_source']['name'] ?? null
            ];
        });
        return $filteredResponse->toArray();
    } catch (\Exception $e) {
        return ['error' => $e->getMessage()];
    }
}



    // public function updateTagScore($productId, $tagName, $incrementBy)
    // {
    //     $client = ClientBuilder::create()->setHosts(['http://laravel-poc-yoss_elasticsearch_1:9200'])->build();
    
    //     $params = [
    //         'index' => 'products',
    //         'id'    => $productId,
    //         'body'  => [
    //             'script' => [
    //                 'source' => "
    //                     for (int i = 0; i < ctx._source.tags.length; i++) {
    //                         if (ctx._source.tags[i].name == params.tagName) {
    //                             ctx._source.tags[i].score += params.incrementBy;
    //                         }
    //                     }
    //                 ",
    //                 'params' => [
    //                     'tagName' => $tagName,
    //                     'incrementBy' => $incrementBy,
    //                 ]
    //             ]
    //         ]
    //     ];
    
    //     $response = $client->update($params);
    //     return $response;
    // }


    

    public function main( $tagsToIncrement)
    {
        // $tagsToIncrement = ['Yellow','Denim'];
        $updatedProducts = [];


        foreach ($tagsToIncrement as $tag) {

            // Obtenir les produits qui ont des tags spécifiques
            $products = $this->getProductsByTags($tag);

            // Mettre à jour le score des tags dans ces produits
            $this->updateScores($products, $tag, $updatedProducts);

        }

    }
    
    function getProductsByTags($tagsToIncrement) {
        $params = [
            'index' => 'products',
            'body'  => [
                'query' => [
                    'nested' => [
                        'path' => 'tags',
                        'query' => [
                            'bool' => [
                                'must' => [
                                    [
                                        'match' => [
                                            'tags.name' => $tagsToIncrement
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
    
        return $this->client->search($params);
    }
    

    function updateScores($products, $tagToIncrement, &$updatedProducts) {
        foreach ($products['hits']['hits'] as $hit) {
            $product = $hit['_source'];
            $productId = $hit['_id'];
    
            // Si le produit a déjà été mis à jour, récupérez les données à jour
            if (isset($updatedProducts[$productId])) {
                $product = $updatedProducts[$productId];
            }
    
            // Augmenter le score du tag de 5 si le tag est dans $tagToIncrement
            foreach ($product['tags'] as &$tag) {
                if ($tag['name'] === $tagToIncrement) {
                    $tag['score'] += 5;
                }
            }
    
            // Stockez les données à jour du produit dans $updatedProducts
            $updatedProducts[$productId] = $product;
    
            // Mettre à jour le produit dans Elasticsearch
            $updateParams = [
                'index' => 'products',
                'id' => $productId,
                'body' => [
                    'doc' => [
                        'tags' => $product['tags']
                    ]
                ]
            ];
    
            $this->client->update($updateParams);
        }
    }
    
    
    



}
