<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'role', 'department', 'id','user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}