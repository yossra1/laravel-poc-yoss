<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Elasticsearch\ClientBuilder;

class CreateElasticsearchIndex extends Command
{
    protected $signature = 'elasticsearch:create-index';
    protected $description = 'Create an Elasticsearch index';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $client = ClientBuilder::create()->setHosts(['http://laravel-poc-yoss_elasticsearch_1:9200'])->build();


        $params = [
            'index' => 'products',
            'body' => [
                'settings' => [
                    'number_of_shards' => 1,
                    'number_of_replicas' => 0
                ],
                'mappings' => [
                    'properties' => [
                        'id' => [
                            'type' => 'integer'
                        ],
                        'name' => [
                            'type' => 'text'
                        ],
                        'tags' => [
                            'type' => 'nested',
                            'properties' => [
                                'name' => [
                                    'type' => 'keyword'
                                ],
                                'score' => [
                                    'type' => 'integer'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $response = $client->indices()->create($params);
        $this->info('Index created successfully.');
    }
}
