<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;



class DeleteAllProducts extends Command
{
    protected $signature = 'products:delete-all';

    protected $description = 'Delete all products from the database';
    
    public function handle()
    {
        // Confirm before deleting
        if ($this->confirm('Do you really want to delete all products? This action is irreversible!')) {
            Product::truncate();
            $this->info('All products have been deleted.');
        } else {
            $this->info('Operation canceled.');
        }
    }
    
}
