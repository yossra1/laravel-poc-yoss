<?php

namespace App\Listeners;

use App\Events\ProductSaved;
use Elasticsearch\ClientBuilder;


class IndexProductInElasticsearch
{
    /**
     * Handle the event.
     *
     * @param  ProductSaved  $event
     * @return void
     */
    public function handle(ProductSaved $event)
    {
        $client = ClientBuilder::create()->setHosts(['http://laravel-poc-yoss_elasticsearch_1:9200'])->build();

        $params = [
            'index' => 'products',
            'id'    => intval($event->product->id),
            'body'  => [
                'id'   => intval($event->product->id),  // Forcer en entier
                'name' => $event->product->name,
                'tags' => $event->product->tags
            ]
        ];

        $client->index($params);
    }
}
