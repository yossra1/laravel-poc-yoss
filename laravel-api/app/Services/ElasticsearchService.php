<?php

namespace App\Services;

use Elasticsearch\ClientBuilder;

class ElasticsearchService
{
    protected $client;

    public function __construct()
    {
        $this->client = \Elasticsearch\ClientBuilder::create()->setHosts(['laravel-poc-yoss_elasticsearch_1:9200'])->build();

        
    }

    /**
     * Index a document in Elasticsearch.
     *
     * @param array $params
     * @return array
     */
    public function index(array $params)
    {
        return $this->client->index($params);
    }

    /**
     * Search documents in Elasticsearch.
     *
     * @param array $params
     * @return array
     */
    public function search(array $params)
    {
        return $this->client->search($params);
    }

    /**
     * Delete a document from Elasticsearch.
     *
     * @param array $params
     * @return array
     */
    public function delete(array $params)
    {
        return $this->client->delete($params);
    }

    // Vous pouvez ajouter d'autres méthodes pour d'autres fonctionnalités d'Elasticsearch si nécessaire.
}
