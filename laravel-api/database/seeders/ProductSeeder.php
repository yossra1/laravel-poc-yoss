<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Créer un produit unique
        Product::create([
            'name' => 'Exemple de produit',
            'description' => 'Description du produit',
            'image' => 'products/1.jpg',
            'price' => 100.50,
            'referenceCode' => 'REF1234',
            'color' => 'Rouge',
            'availability' => 'available',
            'status' => 'published',
            'madeIn' => 'France',
            'type' => 'Electronique',
            'tags' => json_encode(['tag1', 'tag2']),
            'user_id' => 1, // Assurez-vous que l'utilisateur avec l'ID 1 existe
        ]);

        // Si vous voulez générer plusieurs produits de manière aléatoire avec le package Faker
        \App\Models\Product::factory(10)->create(); // Ceci générera 10 produits
    }
}
