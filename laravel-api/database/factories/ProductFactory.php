<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $textileTypes = ['Wool', 'Satin', 'Cotton', 'Polyester', 'Silk', 'Linen', 'Denim', 'Velvet', 'Rayon', 'Nylon'];

        $textileTypes = ['Organic Cotton', 'Linen', 'Recycled Polyester', 'Denim', 'Silk', 'Breathable', 'Sweat-resistant', 'Elastic', 'Water-resistant', 'Thermal', 'OEKO-TEX', 'Fair Trade', 'GOTS (Global Organic Textile Standard)', 'Biodegradable', 'Furniture', 'Clothing', 'Decor', 'Outdoor', 'Woven', 'Knitted', 'Dyed', 'Printed', 'Solid', 'Striped', 'Checked', 'Floral', 'Geometric', 'Summer', 'Winter', 'All-season', 'Mid-season', 'Machine washable', 'Dry clean', 'Low-temperature ironing'];

        
        return [
            'name' => $this->faker->word,
            'description' => $this->faker->sentence,
            'image' => $this->faker->randomElement(array_map(function ($number) {
                return "products/{$number}.jpg";
            }, range(1, 16))),
            'price' => $this->faker->randomFloat(2, 10, 1000),
            'referenceCode' => $this->faker->unique()->ean13,
            'color' => $this->faker->colorName,
            'availability' => $this->faker->randomElement(['available', 'not available']),
            'status' => $this->faker->randomElement(['pending', 'published']),
            'madeIn' => $this->faker->country,
            'type' => $this->faker->randomElement($textileTypes),
            'tags' => json_encode($this->faker->randomElements($textileTypes, $this->faker->numberBetween(3, 7))),  // Encode le tableau en JSON
            'user_id' => 1  // Défini statiquement pour cet exemple.
        ];
    }
}

